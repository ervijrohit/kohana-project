<!Doctype HMLT>
<html lang="en-us">
<head>
    <title>Image Listing Page</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="/assets/js/submitform.js"></script>
</head>
<body>
<div class="container">
    <div class="page-header">
       <h1> Listing of Images </h1>
        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Add Image</button>
    </div>
 <div  id="tbodyis">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Title</th>
                <th>Thumbnail</th>
                <th>Filename</th>
                <th>Date Added</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        <?php
        $listingM = Model::factory('Listing'); // creating an instance of Model
        $recent_posts = $listingM->show_listing();  // calling the Model function to fetch records
            foreach($recent_posts as $key=>$values){

        ?>
            <tr>
                <td><?php print $values['title']; ?></td>
                <td><?php if($values['filename']!=""){?> <img src="<?php print "assets/images/".$values['filename']; ?>" width="100" > <?php } ?></td>
                <td><?php print $values['filename']; ?></td>
                <td><?php print $values['dateadded']; ?></td>
                <td><a onclick="deleterecord(<?php print $values['id']; ?>,'<?php print $values['filename']; ?>');" style="cursor:pointer;">Delete</a> </td>
            </tr>
        <?php
            }
        ?>

        </tbody>
    </table>
 </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <form enctype="multipart/form-data" role="form" id="form">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add New Image</h4>
            </div>
            <div class="modal-body">
                <div id="messages"></div>
                    <div class="form-group">
                        <label for="txtTitle"></label>
                        <input type="text" class="form-control" name="txtTitle" id="txtTitle" placeholder="Title" required>
                    </div>
                    <div class="form-group">
                        <label for="txtFile"></label>
                        <input type="file"  name="file" id="file">
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            </form>
        </div>

    </div>
</div>
</body>

</html>