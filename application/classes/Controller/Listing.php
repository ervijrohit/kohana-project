<?php defined('SYSPATH') or die('No direct script access.');

Class Controller_Listing extends Controller_Template{

    public $template = 'listing'; // setting the view file

    public function action_index(){

    }

    public function action_save(){
        $directory = 'assets/images/';
        $uploaded_file_name = $_FILES['file']['name'];


        $listingM = Model::factory('Listing'); // creating an instance of Model
        $id = $listingM->add_listing($this->request->post('txtTitle')); // Add new record in table

        $ext = substr($uploaded_file_name,strrpos($uploaded_file_name,".")); // getting the extension of the image
        $filename = "Image_".$id[0].$ext; // creating the new custom image name
        $listingM->add_image($filename,$id[0]); // update record with image name
        Upload::save($_FILES['file'], $filename, $directory); // saving the uploaded image
    }

    public function action_delete(){
        $id_to_delete = $this->request->post('id');
        $image_to_delete = $this->request->post('imageis');
        $listingM = Model::factory('Listing'); // creating an instance of Model
        $listingM->delete_listing($id_to_delete); // Deleting the record from table

        @unlink("/assets/images/".$image_to_delete);
    }
}
?>