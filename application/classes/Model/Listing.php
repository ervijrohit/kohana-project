<?php defined('SYSPATH') or die('No direct script access.');

   class Model_Listing extends Model{

       // function to provide listing of records from the images table
       public function show_listing(){

          $query =  DB::select()->from('images'); // select query for feching records
          $result = $query->execute('alternate'); // executing the query using config group 'alternate'
          return $result->as_array();  // return the array of records
       }

       public function add_listing($title){
           $query = DB::insert('images', array('title', 'dateadded'))->values(array($title, date('Y-m-d')));
           $result = $query->execute('alternate'); // executing the query using config group 'alternate'
           return $result;

       }

       public function add_image($file_name,$id){
           $query = DB::update('images')->set(array('filename'=>$file_name))->where('id','=',$id);
           $result = $query->execute('alternate'); // executing the query using config group 'alternate'
       }

       public function delete_listing($id){
           $query = DB::delete('images')->where('id','=',$id); // delete query
           $query->execute('alternate'); // executing the query using config group 'alternate'
       }
   }

?>