$(document).ready(function(){


$('#form').submit(function(e) { // functoin to submit the form values and update listing without refreshing the page

    var form = $(this);
    var formdata = false;
    if(window.FormData){
        formdata = new FormData(form[0]);
    }

    var formAction = form.attr('action');

    $.ajax({
        type        : 'POST',
        url         : 'Listing/save',
        cache       : false,
        data        : formdata ? formdata : form.serialize(),
        contentType : false,
        processData : false,

        success: function(response) {
            if(response != 'error') {
                $("#tbodyis").load(location.href + " #tbodyis");
                $("#myModal .close").click()
            } else {
                $('#messages').addClass('alert alert-danger').text(response);
            }
        }
    });
    e.preventDefault();
});


});
function deleterecord(id,imageis){  // function to delete the record and update the listing without refereshing the page.
        datais = 'id='+id+'&imageis='+imageis;
        
    $.ajax({
        type        : 'POST',
        url         : 'Listing/delete',
        data        : datais,
        cache       : false,
        success: function(response) {

            if(response != 'error') {
                $("#tbodyis").load(location.href + " #tbodyis");

            } else {

            }
        }
    });
}