/*
MySQL Data Transfer
Source Host: localhost
Source Database: kohana
Target Host: localhost
Target Database: kohana
Date: 03-03-2017 03:43:08
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for images
-- ----------------------------
DROP TABLE IF EXISTS `images`;
CREATE TABLE `images` (
  `id` bigint(50) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `dateadded` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records 
-- ----------------------------
INSERT INTO `images` VALUES ('8', 'First Image', 'Image_8.jpg', '2017-03-02');
